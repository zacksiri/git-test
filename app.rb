class Robot
  include SpecialAbility
end

class Animal
  attr_reader :name, :age, :status
  attr_writer :age, :status

  # this is for both attr_reader and attr_writer
  # attr_accessor :age, :status

  def initialize(name, age, status)
    @name = name
    @age = age
    @status = status
  end

  # this is the long form of attr_reader
  # def age
  #   @age
  # end

  # this is the long form of attr_writer
  # def age=(number)
  #   @age = number
  # end
end


module SpecialAbility
  attr_accessor :ability
end


class Mammal < Animal
  attr_accessor :legs, :food

  def initialize(name: name, age: age, status: status, legs: legs)
    super(name, age, status)
    @legs = legs || 0
  end


  def feeds_baby_with
    food = 'milk'
  end
end

class Human < Mammal
  include SpecialAbility

  def initialize(name: name, age: age, status: status)
    super
    @ability = "opposable thumbs"
  end
end

class Bird < Animal
  attr_accessor :wings, :legs
  include SpecialAbility

  class AgeError < StandardError
    define_method(:message) { "Age should be a number!" }
  end

  # TODO: refactor the validation for the age
  # TODO: refactor the Animal class to use keyword arguments

  def initialize(bird_name: '', bird_age: 0, bird_status: '')
    raise Bird::AgeError unless bird_age.kind_of?(Fixnum)
  rescue Bird::AgeError => e
    puts e.message
  else
    super(bird_name, bird_age, bird_status)
    @legs = 2
    @ability = 'fly'
  end
end

class Zoo
  attr_reader :animals

  # TODO: refactor initialize to accept an array of animals
  # zoo = Zoo.new([cat, dog, dolphin])
  def initialize
    @animals = [
                  Mammal.new(name: "Cow", age: 20, status: "Alive", legs: 4),
                  Mammal.new(name: "Cat", age: 4,  status: "Alive", legs: 4), 
                  Mammal.new(name: "Whale", age: 7, status: "Alive", legs: 0),
                  Mammal.new(name: "Human", age: 20, status: "Alive", legs: 2)
                ]
  end

  def search(key, value)
    result = []
    @animals.each do |animal|
      if animal.send(key) == value
        result.push(animal)
      end
      # animal.legs
    end
    result
  end
  #zoo.search('legs')
end

#bird.age 
#zoo.animals

#zoo.search('legs', 4)

# TODO: convert 'hello' into a caplitalized 'Hello'
# TODO: convert 'HELLO' into lowercase 'hello'
# TODO: change 'hello' into 'olleh'
# TODO: ['cat', 'dog', 'snake'] output this array into a string like this cat_dog_snake
# TODO: ['cat', 'dog', 'snake'] output this array into a string like this snake_dog_cat